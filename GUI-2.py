from tkinter import *
from tkinter import filedialog
import tkinter
from PIL import ImageTk, Image
from panel_detection import panel_detection
import matplotlib.pyplot as plt
import cv2
import numpy as np
from os import system


PANEL_PATH = ""

def abrir_archivo():
    path = filedialog.askopenfilename(title="IRT", initialdir="/home/",filetypes=(("Imagenes jpg","*.jpg"),("Imagenes png","*.png")))
    return path

def deteccion_paneles():
    global PANEL_PATH
    PANEL_PATH = abrir_archivo()
    if len(PANEL_PATH)>0:
        panel_detection(PANEL_PATH)
        panel =  Image.open(r'/home/yayokings/Documentos/AI_IRT_Project/TFODCourse/panel.jpg').resize((280,282),Image.ANTIALIAS)
        panel_fondo = ImageTk.PhotoImage(panel)
        panel_label.configure(image=panel_fondo)
        panel_label.image = panel_fondo
        system("python3 extraccion.py "+ PANEL_PATH)
    pass

def posicionamiento_paneles():
    
    
    
    pass
root = Tk()
root.title("IRT")
root.geometry("632x443")
fondo = Image.open(r'/home/yayokings/Documentos/AI_IRT_Project/TFODCourse/GUI_BACKGROUND.jpg')#.resize((1080,654),Image.ANTIALIAS)
boton_imagen = Image.open(r'/home/yayokings/Documentos/AI_IRT_Project/TFODCourse/boton.png').resize((70,70),Image.ANTIALIAS)


imagen_fondo = ImageTk.PhotoImage(fondo)
boton_fondo = ImageTk.PhotoImage(boton_imagen)

label_fondo = Label(root,image=imagen_fondo).place(x=0,y=0)
panel_label = Label(label_fondo)
panel_label.grid(column=2,row=0,padx=90,pady=135)
button_abrir_archivo =  Button(root,image=boton_fondo, text = "Imagen", command= deteccion_paneles)
button_abrir_archivo.grid(column=0,row=0,padx=5,pady=126)
Cargar_imagen = Label(label_fondo,text="Cargar Imagen", font=("Agency FB",7))
Cargar_imagen.grid(column=0,row=0,padx=5,pady=0)
root.mainloop()