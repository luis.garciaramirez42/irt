from tkinter import *
from tkinter import filedialog
import tkinter
from PIL import ImageTk, Image
from panel_detection import panel_detection
from analisis_estadistico import analisis_temperaturas
import matplotlib.pyplot as plt
import cv2
import numpy as np
from os import system


PANEL_PATH = ""

def abrir_archivo():
    path = filedialog.askopenfilename(title="IRT", initialdir="/home/",filetypes=(("Imagenes jpg","*.jpg"),("Imagenes png","*.png")))
    return path

def deteccion_paneles():
    global PANEL_PATH
    PANEL_PATH = abrir_archivo()
    if len(PANEL_PATH)>0:
        panel_detection(PANEL_PATH)

        panel =  Image.open(r'panel.jpg').resize((280,282),Image.ANTIALIAS)
        panel_fondo = ImageTk.PhotoImage(panel)
        panel_label.configure(image=panel_fondo)
        panel_label.image = panel_fondo

        system("python --version")
        system("python3 extraccion.py "+ PANEL_PATH)

        path_arreglo_sano, path_arreglo_condicion_suboptima = analisis_temperaturas(PANEL_PATH)
        panel_sano_imagen =  Image.open("panel_recortes/"+path_arreglo_sano).resize((139,91),Image.ANTIALIAS)
        panel_sano_fondo = ImageTk.PhotoImage(panel_sano_imagen)
        panel_sano.configure(image=panel_sano_fondo)
        panel_sano.image = panel_sano_fondo
        panel_sano.place(x=485, y =231)
        
        if path_arreglo_condicion_suboptima[0]:
            imagen_codiciones_suboptimas=Image.open(path_arreglo_condicion_suboptima[1]).resize((139,91),Image.ANTIALIAS)
            condicion_suboptima_fondo = ImageTk.PhotoImage(imagen_codiciones_suboptimas)
            panel_codiciones_suboptimas.configure(image=condicion_suboptima_fondo)
            panel_codiciones_suboptimas.image = condicion_suboptima_fondo
            panel_codiciones_suboptimas.place(x=485, y =135)

root = Tk()
root.title("IRT")
root.geometry("632x443")
fondo = Image.open(r'GUI_BACKGROUND.jpg')#.resize((1080,654),Image.ANTIALIAS)
boton_imagen = Image.open(r'boton.png').resize((70,70),Image.ANTIALIAS)


imagen_fondo = ImageTk.PhotoImage(fondo)
boton_fondo = ImageTk.PhotoImage(boton_imagen)

label_fondo = Label(root,image=imagen_fondo).place(x=0,y=0)
panel_label = Label(root)
panel_label.grid(column=2,row=0,padx=90,pady=135)
panel_sano = Label(root)
panel_codiciones_suboptimas = Label(root)
#panel_sano.grid(column=3,row=0,padx=0,pady=100)

button_abrir_archivo =  Button(root,image=boton_fondo, text = "Imagen", command= deteccion_paneles)
button_abrir_archivo.grid(column=0,row=0,padx=5,pady=126)
Cargar_imagen = Label(label_fondo,text="Cargar Imagen", font=("Agency FB",7))
Cargar_imagen.grid(column=0,row=0,padx=5,pady=0)
root.mainloop()