import sys
from flirextractor import FlirExtractor

 

if len(sys.argv) == 2:
    foto_name = sys.argv[1]
    print(foto_name)
    with FlirExtractor() as extractor:
        thermal_data = extractor.get_thermal(foto_name)

 

    # save the thermal data as a csv
    import numpy as np
    nombre_sin_jpg = foto_name.replace(".jpg", "")
    print('"'+ nombre_sin_jpg + '"')
    np.savetxt(nombre_sin_jpg + ".csv" , thermal_data, delimiter=",")

 

    # open the thermal data as an image
    from PIL import Image
    from PIL.ImageOps import autocontrast, colorize
    thermal_image = Image.fromarray(thermal_data)
    #thermal_image.show()  # warning, might be quite dark

 

    # use matplotlib to colorize the image
    # Make sure you install matplotlib first
    import matplotlib.pyplot as plt
    plt.matshow(thermal_data)
    colorbar = plt.colorbar()
    colorbar.set_label("Temperature in Degrees Celcius")
    plt.savefig(nombre_sin_jpg+".png")
else:
    print('Argumentos insufucientes')
