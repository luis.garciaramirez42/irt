import csv
import json
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from PIL import Image
import pickle
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split


arreglos_datos_estadisticos = {}

def diferencial_temperatura(datos_panel, datos_imagen,panel_path):

    #determinamos que panel tiene menos dispersion de datos para usarlo como panel sano
    dispersion = 100000000
    arreglo_sano = ""
    for arreglo in datos_panel:

        if dispersion>datos_panel[arreglo]["minimo"]:
            dispersion=datos_panel[arreglo]["minimo"]
            arreglo_sano = arreglo
    print("Arreglo sano: {}".format(arreglo_sano))
    img =Image.open(panel_path)
    x_min = int(datos_imagen[arreglo_sano]['x_min'])
    y_min = int(datos_imagen[arreglo_sano]['y_min'])
    x_max = int(datos_imagen[arreglo_sano]['x_max'])
    y_max = int(datos_imagen[arreglo_sano]['y_max'])
    recorte=(x_min,y_min,x_max,y_max)
    imagen_arreglo= img.crop(recorte)
    imagen_arreglo.save("panel_recortes/{}_SANO.jpg".format(arreglo_sano))
    panel_condicion_suboptima = [False]
    for arreglo in datos_panel:
        diferencial_temperatura = abs(datos_panel[arreglo]["maximo"] - datos_panel[arreglo_sano]["promedio"])
        loaded_model = pickle.load(open("clf_random_forest_model_1.pk1", "rb"))
        if (loaded_model.predict([[1033.731,(datos_panel[arreglo]["maximo"] - datos_panel[arreglo_sano]["promedio"])]])==1):
            print("-->Issue in {}".format(arreglo))
        else:
            print("-->The {} is good".format(arreglo))
        
        print("Diferencial de temperatura entre el arreglo sano {} y el arreglo {} es de {}".format(arreglo_sano, arreglo,diferencial_temperatura))
        if diferencial_temperatura >= 10:
            print("Posible condicion suboptima en el arreglo {}".format(arreglo))
            img =Image.open(panel_path)
            x_min = int(datos_imagen[arreglo]['x_min'])
            y_min = int(datos_imagen[arreglo]['y_min'])
            x_max = int(datos_imagen[arreglo]['x_max'])
            y_max = int(datos_imagen[arreglo]['y_max'])
            recorte=(x_min,y_min,x_max,y_max)
            imagen_arreglo= img.crop(recorte)
            imagen_arreglo.save("panel_recortes/{}_CONDICION_SUBOPTIMA.jpg".format(arreglo))
            panel_condicion_suboptima[0]=True
            panel_condicion_suboptima.append("panel_recortes/{}_CONDICION_SUBOPTIMA.jpg".format(arreglo))

    path_arreglo_sano = arreglo_sano + "_SANO.jpg"
    
    return path_arreglo_sano,panel_condicion_suboptima

def analisis_temperaturas(panel_path):
    print(panel_path)
    print(panel_path.split('.'))
    path_datos = panel_path.split('.')[0]
    with open('posiciones_paneles.json') as file:
        data = json.load(file)
        for panel in data:
            print(panel)
            print('---> Punto inicial (xmin = {}, ymin = {})\n---> Punto final   (xmax = {}, ymax = {})'.format(data[panel]['x_min'],data[panel]['y_min'],data[panel]['x_max'],data[panel]['y_max']))
    #panel = "ARREGL0_FV_#8"
    panel_temperaturas = []
    for panel in data:
        print("Generando archivo .csv para el {}".format(panel))
        csv_nuevo = panel + ".csv"
        with open('{}.csv'.format(path_datos)) as file:
            datos_arreglo_fv = csv.reader(file)
            
            count = 0
            texto_csv = "panel,temperatura\n"
            #print(datos_arreglo_fv[1][1])
            for row in datos_arreglo_fv:
                if count >= data[panel]['y_min'] and count <= data[panel]['y_max']:
                    for i in range(data[panel]['x_min'],data[panel]['x_max']):
                        #panel_temperaturas.append(round(float(row[i]),2))
                        texto_csv += str(count)+ "," + str(round(float(row[i]),2)) + "\n"
                    #print(panel_temperaturas)
                    #panel_temperaturas = []
                count+=1
        file = open(csv_nuevo, "w")
        file.write(texto_csv)
        file.close()
        print("Archivo .csv para el {} generado".format(panel))

    #Usar el rango intercuartil para saber que panel posee menos dispersion de datos
    for panel in data:
        temperaturas = pd.read_csv('{}.csv'.format(panel), engine='python', index_col=0)

        print("Datos sin quitar outliers para {}".format(panel))

        Q1 = temperaturas['temperatura'].quantile(0.25)
        print("--->Primer cuartil {}".format(Q1))

        Q3 = temperaturas['temperatura'].quantile(0.75)
        print("--->Tercer cuartil {}".format(Q3))

        IQR = Q3-Q1
        print("--->Rango intercuartil {}".format(IQR))

        Mediana = temperaturas['temperatura'].median()
        print("--->Mediana {}".format(Mediana))

        Promedio = temperaturas['temperatura'].mean()
        print("--->Promedio {}".format(Promedio))

        Valor_minimo = temperaturas['temperatura'].min()
        print("--->Minimo {}".format(Valor_minimo))

        Valor_maximo = temperaturas['temperatura'].max()
        print("--->Maximo {}".format(Valor_maximo))
        
        BI_Calculado = (Q1 - 1.5*IQR)
        print("--->Bigote inferior {}".format(BI_Calculado))

        BS_Calculado = (Q3 + 3.5*IQR)
        print("--->Bigote superior {}".format(BS_Calculado))

        file = open("{}_2.csv".format(panel), "w")
        texto_csv = "panel,temperatura\n"
        count = 0
        for dato in temperaturas['temperatura']:
            if dato >= BI_Calculado and dato <= BS_Calculado:
                count+=1
                #print(data)
                texto_csv += str(count)+ "," + str(round(dato,2)) + "\n"
        file.write(texto_csv)
        file.close()
        temperaturas = pd.read_csv('{}_2.csv'.format(panel), engine='python', index_col=0)
        
        print("Datos con los quitados outliers para {}".format(panel))

        Q1 = temperaturas['temperatura'].quantile(0.25)
        print("--->Primer cuartil {}".format(Q1))

        Q3 = temperaturas['temperatura'].quantile(0.75)
        print("--->Tercer cuartil {}".format(Q3))

        IQR = Q3-Q1
        print("-->Rango intercuartil {}".format(IQR))

        Mediana = temperaturas['temperatura'].median()
        print("--->Mediana {}".format(Mediana))

        Promedio = temperaturas['temperatura'].mean()
        print("--->Promedio {}".format(Promedio))

        Valor_minimo = temperaturas['temperatura'].min()
        print("--->Minimo {}".format(Valor_minimo))

        Valor_maximo = temperaturas['temperatura'].max()
        print("--->Maximo {}".format(Valor_maximo))
        arreglos_datos_estadisticos[panel] = {}
        arreglos_datos_estadisticos[panel]["rango_intercuartil"] = IQR
        arreglos_datos_estadisticos[panel]["mediana"] = Mediana
        arreglos_datos_estadisticos[panel]["maximo"] = Valor_maximo
        arreglos_datos_estadisticos[panel]["minimo"] = Valor_minimo
        arreglos_datos_estadisticos[panel]["promedio"] = round(Promedio,2)

    for datos in arreglos_datos_estadisticos:
        print("Datos recolectados para {}".format(datos))
        print("--->Rango intercuartil {}".format(arreglos_datos_estadisticos[datos]["rango_intercuartil"]))
        print("--->Mediana {}".format(arreglos_datos_estadisticos[datos]["mediana"]))
        print("--->Promedio {}".format(arreglos_datos_estadisticos[datos]["promedio"] ))
        print("--->Maximo {}".format(arreglos_datos_estadisticos[datos]["maximo"]))
        print("--->Minimo {}".format(arreglos_datos_estadisticos[datos]["minimo"] ))


    return diferencial_temperatura(arreglos_datos_estadisticos, data, panel_path)
